import React, {Component} from 'react'

class RobotForm extends Component {
    
    constructor (props) {
        super(props)
        
        this.state={
            name: '',
            type: '',
            mass: ''
        }
        
        this.handleChange=(evt)=> {
            this.setState({
                    [evt.target.name]: evt.target.value
                })
        }
        
        this.handleClick = () => 
            {
                this.props.onAdd({
                    name : this.state.name,
                    type : this.state.type,
                    mass : this.state.mass

                })
            }
    }
    
    render() {
        return <div>
            <form>
                <label>Name:</label>
                <input type="text" placeholder="name" id="name" name="name" onChange={this.handleChange}/>
                <label>Type:</label>
                <input type="text"  placeholder="type" id="type" name="type" onChange={this.handleChange}/>
                <label>Mass:</label>
                <input type="text"  placeholder="mass" id="mass" name="mass" onChange={this.handleChange}/>
                <input type="button" value="add" onClick={this.handleClick} />
            </form>
        </div>
    }
}

export default RobotForm